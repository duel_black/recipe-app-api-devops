resource "aws_lb" "api" {
  name               = "${local.prefix}-main"
  load_balancer_type = "application"
  subnets = [
    aws_subnet.public_a.id,
    aws_subnet.public_b.id
  ]

  security_groups = [aws_security_group.lb.id]

  tags = local.common_tags
}

resource "aws_lb_target_group" "api" {
  name        = "${local.prefix}-api"
  protocol    = "HTTP"
  vpc_id      = aws_vpc.main.id
  target_type = "ip"
  port        = 8000

  health_check {
    path    = "/api/health-check/"
    matcher = "204"
  }
}

resource "aws_lb_listener" "api" {
  # The listener accepts the request to the load balancer: entry point to LB
  load_balancer_arn = aws_lb.api.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    # Forward this to redirect to https
    type = "redirect"
    redirect {
      status_code = "HTTP_301"
      port        = "443"
      protocol    = "HTTPS"
    }
  }
}

resource "aws_lb_listener" "api_https" {
  # The listener handles https request to your load balancer: entry point to LB
  load_balancer_arn = aws_lb.api.arn
  port              = 443
  protocol          = "HTTPS"

  default_action {
    # Forward the requests to the target group (aws_lb_target_group)
    type             = "forward"
    target_group_arn = aws_lb_target_group.api.arn
  }

  certificate_arn = aws_acm_certificate_validation.cert.certificate_arn
}

resource "aws_security_group" "lb" {
  description = "Allow access to Application Load Balancer"
  name        = "${local.prefix}-lb"
  vpc_id      = aws_vpc.main.id

  ingress {
    # Accept all connections from the internet port 80 into our load balancer
    from_port   = 80
    protocol    = "tcp"
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    # Accept all connections from the internet port 443 into our load balancer
    from_port   = 443
    protocol    = "tcp"
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    # Access our load balancer has to the application
    from_port   = 8000
    protocol    = "tcp"
    to_port     = 8000
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = local.common_tags
}