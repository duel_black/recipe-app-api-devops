# Outputs the RDS address in the console so we can
# grob it to connect to the db through our bastion instance

output "db_host" {
  # The network address for our database
  value = aws_db_instance.main.address
}

output "bastion_host" {
  value = aws_instance.bastion.public_dns
}

output "api_endpoint" {
  # Creates an output value for our load balancer DNS name once it's created by Terraform
  #  value = aws_lb.api.dns_name
  value = aws_route53_record.app.fqdn
}


