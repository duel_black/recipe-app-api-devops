resource "aws_s3_bucket" "app_public_files" {
  # create a new s3 bucket called 'raad-files' (prefix of the project name + -files)
  bucket = "${var.s3_prefix}-${local.prefix}-files"
  acl    = "public-read"
  # Allows you to destroy the bucket with terraform
  force_destroy = true
}
