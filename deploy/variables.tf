variable "prefix" {
  default = "raad"
}

variable "s3_prefix" {
  default = "raad-s3"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "slackbabbath@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "909775216811.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for our proxy server"
  default     = "909775216811.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for django app"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "divvydividends.com"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "apistaging"
    dev        = "apidev"
  }
}