# Recipe App API DevOps Starting Point

Source code for my Udemy course Build a [Backend REST API with Python & Django - Advanced](http://udemy.com/django-python-advanced/).

The course teaches how to build a fully functioning REST API using:

 - Python
 - Django / Django-REST-Framework
 - Docker / Docker-Compose
 - Test Driven Development

### Getting started

To Authenticate into AWS with aws-vault: 
```
aws-vault exec matthew.mansour --duration=12h
```

## Commands
#### Course Reference and Cheat Sheet:
https://gitlab.com/LondonAppDev/recipe-app-api-devops-course-material

https://gitlab.com/LondonAppDev/recipe-app-api-devops-course-material/-/blob/master/commands-reference.md

To build docker image: 
```
docker build .
```

To start project locally, run:

```
docker-compose up
```

To start project locally in uwsgi (as if running on AWS), run:

```
docker-compose -f docker-compose-proxy.yml up
```
Django Admin: http://127.0.0.1/admin/

The API will then be available at http://127.0.0.1:8000/api/recipe/

Running Django management commands in docker, run:

```
docker ps (to get container name)
docker exec -it <container_name> python /app/manage.py <command>
```

Delete All Docker Containers
```
docker rm -vf $(docker ps -aq)
```

Stop All Docker Containers
```
docker kill $(docker ps -aq)
```

Delete All Docker Images
```
docker rmi -f $(docker images -aq)
```
###Docker Compose General

Docker compose management commands with docker-compose
```
docker-compose run --rm app sh -c "python manage.py wait_for_db && python manage.py test && flake8"
```

Docker Compose rebuild single container
```
docker-compose up -d --no-deps --build <service name>
```

Docker Compose stop single container
```
docker-compose stop <service name>
```

Docker compose Run pdb
```
docker-compose run --rm --service-ports <service>
import pdb
pdb.set_trace()
```

Docker compose Logs / Follow
```
docker-composes logs -f -t <name of service>
t = timestamp
f = follow end
```

###Terraform 
#####Terraform will either Create|Modify|Destroy an item
Run terraform with docker compose (to handle terraform version issues):

1. Authenticate with aws-vault (see above)
2. Run (--rm removes docker image after the run)
```
docker-compose -f deploy/docker-compose.yml run --rm terraform <fmt|validate|init|plan|apply|destroy>
```

##### Create a new workspace (environment: dev, staging, prod)
```
docker-compose -f deploy/docker-compose.yml run --rm terraform workspace [list, new]
```

####Terraform Workspaces

#####List all workspaces
```
docker-compose -f deploy/docker-compose.yml run --rm terraform workspace list
```

#####Create a new workspace
```
docker-compose -f deploy/docker-compose.yml run --rm terraform workspace new <workspace_name>
```

### Deploy
Push feature branch to git lab. Deployment pipelines are triggered merge requests

### Bastion
1. Get bastion_host url from the pipeline logs (the apply task) or from AWS/EC2
2. ssh into the bastion with `ssh ec2-user@<host_url>`
3. Authenticate with Docker so we can pull our docker image from ECR
   1. `$(aws ecr get-login --no-include-email --region us-east-1)`
4. Run docker commands for django commands. For example to create a superuser
   1. `docker run -it -e DB_HOST=<DB_HOST_IN_PIPELINE_OUTPUT> -e DB_NAME=<RDS_DB_NAME> -e DB_USER=<USERNAME_IN_VARIABLES_FOR_DB> -e DB_PASS=<IN_GIT_LAB_SETTINGS_CI/CD_VARIABLES> <ECR_IMAGE_URI>:latest sh -c "python manage.py wait_for_db && python manage.py createsuperuser"`
   2. Example below
   3. `docker run -it \
    -e DB_HOST=<DB_HOST> \
    -e DB_NAME=recipe \
    -e DB_USER=recipeapp \
    -e DB_PASS=<DB_PASS> \
    <ECR_REPO>:latest \
    sh -c "python manage.py wait_for_db && python manage.py createsuperuser"`
5. Test ECS Service
   1. In AWS goto ECS -> Cluster -> Task, expand and use the <PUBLIC_IP>:8000 to see app

